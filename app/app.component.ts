import { Component } from '@angular/core';


@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    styleUrls: ['app/app.component.css']
})
export class AppComponent { 

    month_2016: string[];
    month_2015: string[];
    month_2014: string[];

    selected:string[];

    constructor() {

        this.month_2016 = ["jan 2016","fab 2016", "mar 2016", "apr 2016","jan 2016","fab 2016"];
        this.month_2015 = ["jan 2015","fab 2015", "mar 2015", "apr 2015", "mar 2015", "apr 2015"];

        this.selected = this.month_2016;
    }


    ChangeMonth(obj:string):void {
        console.log(obj.value);
        

        switch(obj.value) {

            case 2016: { this.selected = this.month_2016 } break;
            case 2015: { this.selected = this.month_2015 } break;
        }

    }


}